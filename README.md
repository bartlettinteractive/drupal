# Template for Drupal 8 projects

This project provides a template for starting a Drupal 8 project with composer.

## Usage

Run the following command from the directory you would like the project in.

```
composer create-project bartlettinteractive/drupal . --stability dev --no-interaction
```

## Installing modules

Install Drupal modules with composer

```
composer require drupal/MODULE
```

## Updating Drupal core

Run `composer update drupal/core --with-dependencies`
